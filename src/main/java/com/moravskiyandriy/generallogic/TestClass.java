package com.moravskiyandriy.generallogic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class TestClass {
    private static final Logger logger = LogManager.getLogger(TestClass.class);

    @CustomAnnotation(intField = 44)
    private final int value;

    private final String data;

    public TestClass(int value, String data) {
        this.value = value;
        this.data = data;
    }

    public int getValue() {
        return value;
    }

    public String getData() {
        return data;
    }

    public void SomeMethod1() {
        logger.info("SomeMethod1 run.");
    }

    @CustomAnnotation
    public void SomeMethod2() {
        logger.info("SomeMethod2 run.");
    }

    @CustomAnnotation(intField = 100)
    public void SomeMethod3() {
        logger.info("SomeMethod3 run.");
    }

    public void myMethod(String a, int... args) {
        logger.info("myMethod(String a, int ... args) invoked." + " a= " + a + ", args len: " + args.length);
    }

    public void myMethod(String... args) {
        logger.info("myMethod(String ... args) invoked, args len: " + args.length);
    }
}
