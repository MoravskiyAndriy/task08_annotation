package com.moravskiyandriy.generallogic;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.METHOD, ElementType.FIELD})
public @interface CustomAnnotation {
    int intField() default 10;
}
