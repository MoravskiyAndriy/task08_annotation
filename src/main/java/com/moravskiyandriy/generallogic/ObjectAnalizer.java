package com.moravskiyandriy.generallogic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class ObjectAnalizer {
    private static final Logger logger = LogManager.getLogger(ObjectAnalizer.class);
    private Object object;

    public ObjectAnalizer(Object object) {
        this.object = object;
    }

    public void getInfo() {
        Class investigator = object.getClass();
        Method[] declaredMethods = investigator.getDeclaredMethods();
        Field[] declaredFields = investigator.getDeclaredFields();
        logger.info("Class: " + investigator.getName() + "\nMethods:");
        for (Method m : declaredMethods) {
            logger.info("\nname: " + m.getName() + "\nannotations: ");
            Arrays.stream(m.getAnnotations()).forEach(a -> logger.info(a.annotationType().getName()));
            logger.info("return type:" + m.getReturnType());
        }
        logger.info("\nFields:");
        for (Field f : declaredFields) {
            logger.info("\nname: " + f.getName() + "\nannotations: ");
            Arrays.stream(f.getAnnotations()).forEach(a -> logger.info(a.annotationType().getName()));
            logger.info("type:" + f.getType());
        }
    }
}
