package com.moravskiyandriy;

import com.moravskiyandriy.generallogic.CustomAnnotation;
import com.moravskiyandriy.generallogic.ObjectAnalizer;
import com.moravskiyandriy.generallogic.TestClass;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Application {
    private static final Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        TestClass tc = new TestClass(33, "line");
        Class investigator = tc.getClass();

        annotationInfo(tc, investigator);

        methodsInvoking(tc, investigator);

        specialMethodsInvoking(tc, investigator);

        logger.info("\n\nObject analyze:");
        ObjectAnalizer analizer = new ObjectAnalizer(tc);
        analizer.getInfo();
    }

    private static void specialMethodsInvoking(TestClass tc, Class investigator) {
        try {
            Method accessor = investigator.getMethod("myMethod", String.class, int[].class);
            accessor.setAccessible(true);
            int[] intArr = {1, 3, 5};
            accessor.invoke(tc, "hi", intArr);
            Method accessor2 = investigator.getMethod("myMethod", String[].class);
            accessor.setAccessible(true);
            String[] StringArr = {"one", "two"};
            accessor2.invoke(tc, new Object[]{StringArr});
        } catch (NoSuchMethodException e) {
            logger.info("No such method...");
        } catch (IllegalAccessException e) {
            logger.info("Illegal access...");
        } catch (InvocationTargetException e) {
            logger.info("Invocation target...");
        }
    }

    private static void methodsInvoking(TestClass tc, Class investigator) {
        logger.info("\nMethods Invoking:");
        try {
            Method accessor = investigator.getDeclaredMethod("SomeMethod1");
            accessor.setAccessible(true);
            accessor.invoke(tc);
            Method accessor2 = investigator.getDeclaredMethod("SomeMethod2");
            accessor2.setAccessible(true);
            accessor2.invoke(tc);
            Method accessor3 = investigator.getDeclaredMethod("SomeMethod3");
            accessor3.setAccessible(true);
            accessor3.invoke(tc);
        } catch (NoSuchMethodException e) {
            logger.info("No such method...");
        } catch (IllegalAccessException e) {
            logger.info("Illegal access...");
        } catch (InvocationTargetException e) {
            logger.info("Invocation target...");
        }
    }

    private static void annotationInfo(TestClass tc, Class investigator) {
        List<Field> someFields = Arrays.stream(investigator.getDeclaredFields())
                .filter(f -> Arrays.stream(f.getAnnotations())
                        .map(Annotation::annotationType)
                        .collect(Collectors.toList())
                        .contains(CustomAnnotation.class))
                .collect(Collectors.toList());

        if (!someFields.isEmpty()) {
            for (Field someField : someFields) {
                someField.setAccessible(true);
                try {
                    Object value = someField.get(tc);
                    logger.info(someField.getName() + "= " + value.toString());
                    CustomAnnotation ca = someField.getAnnotation(CustomAnnotation.class);
                    logger.info("Annotation field= " + ca.intField());
                    someField.set(tc, 78);
                    logger.info("After setting feld: ");
                    Object value2 = someField.get(tc);
                    logger.info(someField.getName() + "= " + value2.toString());
                    CustomAnnotation ca2 = someField.getAnnotation(CustomAnnotation.class);
                    logger.info("Annotation field= " + ca2.intField());

                } catch (Exception e) {
                    logger.info("No such field...");
                }
            }
        }
    }
}
